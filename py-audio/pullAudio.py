from pytube import YouTube
from pytube import Channel
from pytube import Playlist
import os, time

destination = "/var/data/telegrambot/pravachan/audio/"
playlists = {
"jan":"https://www.youtube.com/playlist?list=PLAxUdkn_QMmyUimLMCtSb2Shx_ifV_Cu7",
"feb":"https://www.youtube.com/playlist?list=PLAxUdkn_QMmxLxJlOByFa81WYT1OhtY0w",
"mar":"https://www.youtube.com/playlist?list=PLAxUdkn_QMmwW3gIxsElri4WyU3Poh2xG",
"apr":"https://www.youtube.com/playlist?list=PLAxUdkn_QMmw2K5lqteCvLl4NdDNUYZCF",
"may":"https://www.youtube.com/playlist?list=PLAxUdkn_QMmw8_5rix0TtyE8UUNHN1Ppp",
"june":"https://www.youtube.com/playlist?list=PLAxUdkn_QMmyHqb60sNEf_LCXxSPxChZv",
"july":"https://www.youtube.com/playlist?list=PLAxUdkn_QMmzswMzSUfRTvMakcmUvrvMI",
"aug":"https://www.youtube.com/playlist?list=PLAxUdkn_QMmyYhmpV09LqgknY4oTuvDhC",
"sep":"https://www.youtube.com/playlist?list=PLAxUdkn_QMmz0lVJFh8rVbKLfVVagK1qL",
"oct":"https://www.youtube.com/playlist?list=PLAxUdkn_QMmxegBJID3jraNM2YQiB2Z7w",
"nov":"https://www.youtube.com/playlist?list=PLAxUdkn_QMmx9_NCeacCKSi0KdudFj36O",
"dec":"https://www.youtube.com/playlist?list=PLAxUdkn_QMmwCHWm_SxD9Sy6FsjvvFEsq",
"gurupornima": "https://www.youtube.com/playlist?list=PLAxUdkn_QMmzWRReVZGjbmX927rHIDY5c",
"ramnavmi": "https://www.youtube.com/playlist?list=PLAxUdkn_QMmzD6Qgys_rxSjkxATHWerin"
}

for key, link in playlists.items():
    p=Playlist(link)
    for i,yt in enumerate(p.videos):
        print(f"**************** {371-i+1}")
        titleSplit = yt.title.split(" ")
        audioDirPath = destination + key
        
        if not os.path.exists(audioDirPath):
            os.makedirs(audioDirPath)
            print(f"The new directory {audioDirPath} is created!")

        stream = yt.streams.filter(only_audio=True).first()
        out_file = stream.download(output_path="/tmp/")
        os.rename(out_file, audioDirPath + "/" + titleSplit[-2] + ".mp3")
        print(f"downlaodedfile {yt.title}")
        time.sleep(1)
