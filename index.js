/*jshint esversion: 8 */

const TG = require('telegram-bot-api');
// const fs = require('fs');
const { S3Client, GetObjectCommand } = require("@aws-sdk/client-s3");
const bareBonesS3 = new S3Client();
const bucketName = process.env.ASSET_BUCKET_NAME; // bucket name from env var

const streamToString = (stream) =>
	new Promise((resolve, reject) => {
		const chunks = [];
		stream.on("data", (chunk) => chunks.push(chunk));
		stream.on("error", reject);
		stream.on("end", () => resolve(Buffer.concat(chunks).toString("utf8")));
	});

// const streamToFile = (stream, filePath) =>
// 	new Promise((resolve, reject) => {
// 		try {
// 			const outputStream = fs.createWriteStream(filePath);
// 			stream.pipe(outputStream);
// 			outputStream.on('finish', () => {
// 				resolve(true);
// 			});
// 		} catch (error) {
// 			reject(error);
// 		}
// 	});

// This is entry point of the lambada function
// exports handler = async () => {
exports.handler = async () => {
	try {
		console.log('sending message'); //All console logs are gone to CW logs

		//  get date and month, this need to find exact files in s3 bucket. There is a standard naming convention followed to store the files
		const months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
		const date = new Date();
		const monthNumber = date.getMonth();

		const day = date.getDate().toString().padStart(2, "0");

		// Find caption data
		const captionData = await bareBonesS3.send(new GetObjectCommand({
			Bucket: bucketName,
			Key: `text/${months[monthNumber]}/${day}.caption.txt`,
		}));
		const caption = await streamToString(captionData.Body);

		// Find title
		const titleData = await bareBonesS3.send(new GetObjectCommand({
			Bucket: bucketName,
			Key: `text/${months[monthNumber]}/${day}.title.txt`,
		}));
		const title = await streamToString(titleData.Body);

		// find actual message
		const messageData = await bareBonesS3.send(new GetObjectCommand({
			Bucket: bucketName,
			Key: `text/${months[monthNumber]}/${day}.message.txt`,
		}));
		const message = await streamToString(messageData.Body);

		// Initialise Telegram API
		const api = new TG({ token: process.env.TELEGRAM_API_TOKEN });

		// Send text message to telegram chat/group
		await api.sendMessage({
			chat_id: process.env.CHAT_ID,
			text: message,
			parse_mode: 'MarkdownV2'
		});

		console.log("Sent text!!!");

		// console.log('downloading audio file');
		// const data = await bareBonesS3.send(new GetObjectCommand({
		// 	Bucket: bucketName,
		// 	Key: `audio/${months[monthNumber]}/${day}.mp3`,
		// }));
		// await streamToFile(data.Body, `/tmp/${months[monthNumber]}${day}.mp3`);
		// console.log("downloaded audio file");

		// send audio files to chat
		// const audioURL = `https://pravachane.s3.ap-south-1.amazonaws.com/audio/${months[monthNumber]}/${day}.mp3`;
		// console.log(audioURL);

		// IMP: performer and title is not showing if we send file by URL. But to save cost and to deal with timeout issue, we need to pass URL only
		await api.sendAudio({
			chat_id: process.env.CHAT_ID,
			caption: caption,
			performer: 'ब्रह्मचैतन्य !',
			title: title,
			audio: `https://pravachane.s3.ap-south-1.amazonaws.com/audio/${months[monthNumber]}/${day}.mp3`
			// thumb: fs.createReadStream('maharaj.jpeg'),
			
		});

		console.log("Sent audio!!!");
	} catch (error) {
		console.log(error);
	}
}