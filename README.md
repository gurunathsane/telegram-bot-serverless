# Serverless, Telegram bot, Gitlab-ci

## Introduction
This is a [NodeJS](https://nodejs.org/) code that is deployed in [AWS Lambda](https://aws.amazon.com/lambda/) using the [serverless framework](https://www.serverless.com/). The purpose of this code is to fetch one audio file and one text file from an AWS S3 bucket and send it to a Telegram group every day with [telegram bot API](https://core.telegram.org/bots/api). The content of the files is meant to provide inspiration for each day, and it is delivered in the [Marathi language](https://en.wikipedia.org/wiki/Marathi_language). This automated process ensures that the group members receive fresh, uplifting content each morning without any manual intervention.

[gitlab-ci](https://docs.gitlab.com/ee/ci/) helps us to deploy all changes whenever I made some updates in the code.

## File structure
- `index.js`:  This is main JS file which pulls files based on date from S3 bucket and send it to telegram group with help of telegram bot API. The file has enough comments to explain the code flow.
- `package.json`: package.json is a configuration file used in Node.js applications to specify the dependencies, metadata, and other important information about the project. It is essentially a JSON file that contains all the relevant details about the project, such as the name, version, description, entry point, license, and dependencies required for the application to run.

  This file specifies the dependencies for this project The important dependencies are:
  - `@aws-sdk/client-s3`: this package require to pull files from s3.
  - `serverless`: the serverless framework, which simplify deployment process.
  - `telegram-bot-api`: this helps us to send files to telegram chanel/group. 
- `serverless.yml`: serverless.yaml is a configuration file used by the Serverless Framework. This file is written in YAML (YAML Ain't Markup Language), a human-readable data serialization format. It defines the entire serverless infrastructure of an application. Enough comments are added in the file to understand this deeply.
- `.env.*`: these are env files. All required env variables are stored in these files. * here is the env name. for example. if we want to deploy in dev env then we need to create .env.dev file (example file [env.example](./env.example)) and add correct values. Or we need to export variables with all keys with correct values 
- `gitlab-ci.yml`: This is a YAML file used by GitLab CI/CD pipelines to define the stages, jobs, and tasks required to build, test, and deploy a project. File has enough comments to make it self explanatory. 
- `eslintrc.yml`: This is a configuration file used by the ESLint tool, which is a popular static code analysis tool for JavaScript.ESLint is used to identify and report on patterns found in JavaScript code, and can help developers ensure that their code follows best practices, is free from errors, and is consistent in style and formatting. The eslint.yml file is used to configure the behavior of the ESLint tool, including which rules to apply, which plugins to use, and how to handle errors and warnings.

## Serverless Framework
[The Serverless Framework](https://www.serverless.com/) is an open-source framework that allows developers to build and deploy applications on serverless architectures. It provides a set of tools and abstractions for managing the infrastructure required for running serverless applications.

The framework is designed to work with various cloud providers, including Amazon Web Services (AWS), Microsoft Azure, Google Cloud Platform (GCP), and more. With the Serverless Framework, developers can define their application's resources and configuration using a YAML-based configuration file, which is used to create and manage the necessary cloud resources.

The Serverless Framework also provides a command-line interface (CLI) that simplifies the deployment process, making it easier for developers to deploy and manage their serverless applications. The CLI allows developers to create, test, and deploy serverless functions and resources, and it provides features like automatic scaling, monitoring, and error reporting. Overall, the Serverless Framework provides a convenient and efficient way to build and deploy serverless applications.

This repository deploy serverless in AWS cloud. The [serverless.yml](./serverless.yml) file is config file. We can setup env variables mentioned in [env.example](./env.example) or we can make a copy of this file and put correct values. Copied file name will be .env.{stage name}. 

We can deploy application with command. This command will create/update existing assets with help of cloudformation. 

```
serverless deploy --stage={stage name}
```

We can delete all assets from cloud by running command
```
serverless remove --stage={stage name}
```
_Please note: this will use aws cred from: env_variables or from aws config file or from VM role_

### benefits of serverless
Here are some high-level benefits of using the Serverless Framework:

- `Scalability`: Serverless architectures can scale automatically to meet demand. The Serverless Framework provides easy integration with auto-scaling features of cloud providers, allowing applications to scale seamlessly.

- `Reduced infrastructure management`: By abstracting away server management, developers can focus on building applications rather than worrying about infrastructure maintenance.

- `Cost-effective`: With serverless architectures, you only pay for what you use. The Serverless Framework provides cost monitoring and optimization features, allowing you to keep your cloud bill under control.

- `Faster time-to-market`: The Serverless Framework enables rapid development and deployment of applications, allowing developers to bring new features and products to market more quickly.

- `Vendor-agnostic`: The Serverless Framework supports multiple cloud providers, so you can choose the one that best fits your needs without being locked in to a single vendor.

## Telegram Bot
[Telegram Bot API](https://core.telegram.org/bots/api) is a set of tools and interfaces provided by Telegram for developers to create and manage Telegram bots. The Telegram Bot API allows developers to communicate with the Telegram servers and to interact with Telegram users through their bots.

The Telegram Bot API provides a wide range of functionality for bot development, including sending and receiving messages, managing chat groups, setting up custom keyboards, handling user authentication, and even sending files and multimedia content. Additionally, the API provides webhooks that can be used to receive updates and notifications from the Telegram servers when users interact with the bot.

Telegram Bot API is available in several programming languages, including Python, Java, Node.js, Ruby, and others, making it easy for developers to create bots in their preferred programming language. In this repository we used Telegram Bot API node package which is defined in [package.json](./package.json).

## Gitlab CI
[GitLab CI/CD](https://docs.gitlab.com/ee/ci/) (Continuous Integration and Continuous Deployment) pipelines are a set of automated processes that help developers build, test, and deploy their applications more efficiently. GitLab is a popular Git repository manager that provides a complete CI/CD solution.

With GitLab CI/CD pipelines, developers can define the tasks and stages needed to build, test, and deploy their code. These tasks can include compiling the code, running unit tests, and creating deployment packages. Pipelines can also include approval steps to ensure that changes are reviewed before they are deployed.

GitLab pipelines use a YAML file to define the pipeline stages and jobs. Jobs are the individual tasks that make up a pipeline, such as building the code, running tests, and deploying the application. Stages are groups of related jobs that are executed in a specific order.

GitLab pipelines also provide various features like parallel job execution, artifacts management, manual triggers, and conditional job execution. With these features, developers can optimize their pipelines to run faster and more efficiently.

## Setup this project
In order to setup this project there are few pre-requisite. 
- Node.js v16.0.0 or later
- Telegram account
- AWS account

### Install Serverless
Serverless package is listed in [package.json](./package.json). Running command `npm install` from project directory will install serverless framework. For advance use cases we can install serverless package globally by running command `npm i -g serverless`

### Create Telegram bot
To Create telegram bot. First open Telegram app from mobile device/web version. Find telegram bot called [@BotFather](https://web.telegram.org/#/im?p=@BotFather) and send message `/newbot`. This will generate a telegram token. We need to pass this token to lambda function. This example code will access this token from SSM parameter store. Create a new parameter in same region by running command: 
```
aws ssm put-parameter \
    --name="TELEGRAM_API_TOKEN" \
    --value="{TELEGRAM_TOKEN}" \
    --type String
```
_replace TELEGRAM_TOKEN with correct token_

### Create env file. 
We need to create env file. Make a copy [env.example](./env.example) file and update the content in it.
| NAME | Required | Description | default value | 
| --- | --- | --- | --- | 
| CHAT_ID | YES | The chat ID where you want to send a message. | nil | 
| ENABLE_CRON | No | whether to enable CRON job or not on this CHAT| false | 
| ASSET_BUCKET_NAME | YES | Where to find assets in the bucket. There is standard naming convention followed to keep files. | nil | 
| REGION| No | The region in which lambada function will be deployed | ap-south-1 | 
| memorySize| No | The memory size allocated to lambda function (in MB) | 128 |
| timeout | No | Time out set for lambda function | 10 |



## Schedule event type

This examples defines one function, `telegramBot`, which is  triggered by an event of `schedule` type, which is used for configuring functions to be executed at specific time or in specific intervals. For detailed information about `schedule` event, please refer to corresponding section of Serverless [docs](https://serverless.com/framework/docs/providers/aws/events/schedule/).

When defining `schedule` events, we need to use `rate` or `cron` expression syntax. In this example we used `cron`

### Cron expressions syntax

```
cron(Minutes Hours Day-of-month Month Day-of-week Year)
```

All fields are required and time zone is UTC only.

| Field         | Values         | Wildcards     |
| ------------- |:--------------:|:-------------:|
| Minutes       | 0-59           | , - * /       |
| Hours         | 0-23           | , - * /       |
| Day-of-month  | 1-31           | , - * ? / L W |
| Month         | 1-12 or JAN-DEC| , - * /       |
| Day-of-week   | 1-7 or SUN-SAT | , - * ? / L # |
| Year          | 192199      | , - * /       |

In below example, we use `cron` syntax to define `schedule` event that will trigger our `cronHandler` function every second minute every Monday through Friday

```yml
functions:
  cronHandler:
    handler: handler.run
    events:
      - schedule: 
          rate: cron(1 1 * * ? *)
          enabled: ${env:ENABLE_CRON}
```

Detailed information about cron expressions in available in official [AWS docs](https://docs.aws.amazon.com/AmazonCloudWatch/latest/events/ScheduledEvents.html#CronExpressions).

## Local invocation

In order to test out your functions locally, you can invoke them with the following command:

```
serverless invoke local --function telegramBot
```