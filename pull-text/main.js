const fs = require('fs');
const axios = require('axios');
const parse = require('node-html-parser').parse;

async function main(){
	const months = ['jan', 'feb', 'mar', 'apr', 'may', 'jun', 'jul', 'aug', 'sep', 'oct', 'nov', 'dec'];
	for(let i=1;i<=31;i++){
		for(let j=0;j<months.length;j++){
		

			let day = i.toString().padStart(2, "0");
			const url = `http://satsangdhara.net/shri/${months[j]}${day}.htm`;
			console.log(url);
			try{
				console.log(`*************** getting ${url}`);
				const response = await axios.get(url);

				console.log('parsing');
				const root = parse(response.data);
				let title = root.querySelectorAll("h3")[0].innerHTML;
				let performer = 'ब्रह्मचैतन्य !'
				let dateText = root.querySelectorAll("h1")[1].innerHTML;
				let caption = root.querySelectorAll("h4")[0].innerText;
				let messageParts = root.querySelectorAll("p");
				let message = `!*श्री राम जय राम जय जय राम !*\r\n*${caption}* \r\n \r\n`;
				for(let k=0; k < messageParts.length;k++){
					message += `${messageParts[k].innerHTML}\r\n`;
				}
				message += `\r\n_${title}_ \r\n*जानकी जीवन स्मरण जय जय राम !!!*\r\n${performer}`;
				message = message.replace(/\./g, '\\.').replace(/\!/g, '\\!'); // eslint-disable-line no-useless-escape

				let jsondata = JSON.stringify({
					title: title,
					performer: performer,
					dateText: dateText,
					caption: caption,
					message: message
				})
				console.log('dumping');
				fs.writeFileSync(`data/${months[j]}${day}.json`, jsondata);
				console.log('done');
				// break;
			}catch(err){
				console.log(`Failed to pull file ${url}`)
			}

		}
	}
}

main();